package rot13Server.rot13Server;

public class Result {

    private String result;

    public Result ( String message) { result = message; }

    public Result(){}

    public void setResult(String message) {
        result = message;
    }

    public String getResult() {
        return result;
    }


    @Override
    public String toString()
    {
        return "Result{ result='" + result + '\'' + "}";
    }
}