package rot13Server.rot13Server;

import java.io.Serializable;

public class EncodeMessage {

    private String encodeMessage;

    public EncodeMessage(String encodeMessage) {
        this.encodeMessage = encodeMessage;
    }

    public EncodeMessage() {
    }

    public void encoding(String encodeMsg) {
        encodeMessage = Rot13script.coding(encodeMsg);
    }

    public String getEncodeMessage() {
        return encodeMessage;
    }

    public void setDecodeMessage(String encodeMessage) {
        this.encodeMessage = encodeMessage;
    }
}