package rot13Server.rot13Server;

public class DecodeMessage {

    private String decodeMessage;

    public DecodeMessage(String decodeMessage) {
        this.decodeMessage = decodeMessage;
    }

    public DecodeMessage() {
    }

    public void decoding(String decodeMsg) {
        decodeMessage = Rot13script.coding(decodeMsg);
    }

    public String getDecodeMessage() {
        return decodeMessage;
    }

    public void setDecodeMessage(String decodeMessage) {
        this.decodeMessage = decodeMessage;
    }
}