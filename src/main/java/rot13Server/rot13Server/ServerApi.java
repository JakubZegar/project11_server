package rot13Server.rot13Server;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/api")
public class ServerApi {

    @CrossOrigin
    @RequestMapping(value = "/rot13/encode", method = RequestMethod.POST)
    public ResponseEntity<?> getEncodeMessage(@RequestBody EncodeMessage message) {

        message.encoding(message.getEncodeMessage());

        Result result = new Result(message.getEncodeMessage());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/rot13/decode", method = RequestMethod.POST)
    public ResponseEntity<?> getDecodeMessage(@RequestBody DecodeMessage message) {

        message.decoding(message.getDecodeMessage());

        Result result = new Result(message.getDecodeMessage());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/server-test", method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> serverTest() {

        Map<String, String> serverTestMessage = new HashMap<>();
        serverTestMessage.put("server-status", "RUN :-)");

        return new ResponseEntity<>(serverTestMessage, HttpStatus.OK);
    }
}


