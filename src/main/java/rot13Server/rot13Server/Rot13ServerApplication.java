package rot13Server.rot13Server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Rot13ServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Rot13ServerApplication.class, args);
	}
}
