package rot13Server.rot13Server;

public class Rot13script {

    public static String coding(String inMsg) {
        String outMsg="";
        for (int i = 0; i < inMsg.length(); i++) {
            char c = inMsg.charAt(i);
            if       (c >= 'a' && c <= 'm') c += 13;
            else if  (c >= 'A' && c <= 'M') c += 13;
            else if  (c >= 'n' && c <= 'z') c -= 13;
            else if  (c >= 'N' && c <= 'Z') c -= 13;
            else c = c;
            outMsg+=c;
        }

        return outMsg;
    }
}
